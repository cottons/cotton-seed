'use strict';
var yeoman = require('yeoman-generator');
var yosay = require('yosay');

module.exports = yeoman.generators.NamedBase.extend({

  initializing: function() {
    this.name = 'cotton-' + this.name;
  },

  promptUser: function() {
    var done = this.async();

    // have Yeoman greet the user
    this.log(yosay(
      'Welcome to the cool ' + this.name + ' generator!'
    ));

    var prompts = [{
      name: 'description',
      message: 'Write a description for the element'
    }];

    this.prompt(prompts, function (props) {
      this.elementDescription = props.description;

      done();
    }.bind(this));
  },

  writing: {

    scaffoldFolders: function() {
      this.mkdir(this.name);
      this.mkdir(this.name + '/demo');
    },
    app: function () {
      var context = {element_name: this.name, element_description: this.elementDescription};
      this.copy('bowerrc', '.bowerrc');
      this.copy('gitignore', '.gitignore');
      this.template('demo/index.html', this.name + '/demo/index.html', context);
      this.template('demo/server.js', this.name + '/demo/server.js', context);
      this.template('bower.json', this.name + '/bower.json', context);
      this.template('package.json', this.name + '/package.json', context);
      this.template('cotton-seed.html', this.name + '/' + this.name + '.html', context);
    }
  },
  install: function () {
    process.chdir(process.cwd() + '/' + this.name);
    this.installDependencies({cwd: this.name});
  }
});
